package com.np.servicefactory.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class UploadedFiles implements Serializable{

	private static final long serialVersionUID = -7696473856568743375L;
	
	Set<String> dpaFiles = new HashSet<String>();
	Set<String> testReqFiles = new HashSet<String>();
	Set<String> testResFiles = new HashSet<String>();
	Set<String> xsltFiles = new HashSet<String>();
	
	public void addFileToDPASet(String fileName) {
		dpaFiles.add(fileName);
	}
	
	public void addFileToTestReqSet(String fileName) {
		testReqFiles.add(fileName);
	}
	
	
	public void addFileToTestResSet(String fileName) {
		testResFiles.add(fileName);
	}
	
	public void addFileToXSLTSet(String fileName) {
		xsltFiles.add(fileName);
	}
	
	
	public void removeFileToXSLTSet(String fileName) {
		xsltFiles.remove(fileName);
	}
	
	public void removeFileToDPASet(String fileName) {
		dpaFiles.remove(fileName);
	}
	
	public void removeFileToTestReqSet(String fileName) {
		testReqFiles.remove(fileName);
	}
	
	
	public void removeFileToTestResSet(String fileName) {
		testResFiles.remove(fileName);
	}
	
	
	
	
	
	public Set<String> getDpaFiles() {
		return dpaFiles;
	}
	public void setDpaFiles(Set<String> dpaFiles) {
		this.dpaFiles = dpaFiles;
	}
	public Set<String> getTestReqFiles() {
		return testReqFiles;
	}
	public void setTestReqFiles(Set<String> testReqFiles) {
		this.testReqFiles = testReqFiles;
	}
	public Set<String> getTestResFiles() {
		return testResFiles;
	}
	public void setTestResFiles(Set<String> testResFiles) {
		this.testResFiles = testResFiles;
	}
	public Set<String> getXsltFiles() {
		return xsltFiles;
	}
	public void setXsltFiles(Set<String> xsltFiles) {
		this.xsltFiles = xsltFiles;
	}
	
}
